**GitLab Security Workshop 2021-12-01**

Select the PDF Guide - GitLab_Security_Workshop.pdf above, **download** it using the down arrow icon above the pdf file on the top right. 

![](Guide_Screen.png)
